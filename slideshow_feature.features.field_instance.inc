<?php
/**
 * @file
 * slideshow_feature.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function slideshow_feature_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'field_collection_item-field_slide_content-field_slide_image'
  $field_instances['field_collection_item-field_slide_content-field_slide_image'] = array(
    'bundle' => 'field_slide_content',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => 'large',
          'linked_field' => array(
            'advanced' => array(
              'class' => '',
              'rel' => '',
              'target' => '',
              'text' => '',
              'title' => '',
            ),
            'destination' => '[field_collection_item:field-slide-link]',
            'linked' => 1,
          ),
        ),
        'type' => 'image',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => 'theme_ds_field_reset',
    'entity_type' => 'field_collection_item',
    'field_name' => 'field_slide_image',
    'label' => 'Image',
    'required' => 1,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'field_collection_item-field_slide_content-field_slide_link'
  $field_instances['field_collection_item-field_slide_content-field_slide_link'] = array(
    'bundle' => 'field_slide_content',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 2,
      ),
    ),
    'ds_extras_field_template' => 'theme_ds_field_reset',
    'entity_type' => 'field_collection_item',
    'field_name' => 'field_slide_link',
    'label' => 'Link',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'field_collection_item-field_slide_content-field_slide_text'
  $field_instances['field_collection_item-field_slide_content-field_slide_text'] = array(
    'bundle' => 'field_slide_content',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'linked_field' => array(
            'advanced' => array(
              'class' => '',
              'rel' => '',
              'target' => '',
              'text' => '',
              'title' => '',
            ),
            'destination' => '',
            'linked' => 0,
          ),
        ),
        'type' => 'text_default',
        'weight' => 1,
      ),
    ),
    'ds_extras_field_template' => 'theme_ds_field_reset',
    'entity_type' => 'field_collection_item',
    'field_name' => 'field_slide_text',
    'label' => 'Text',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'node-slideshow-field_slide_content'
  $field_instances['node-slideshow-field_slide_content'] = array(
    'bundle' => 'slideshow',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'cycle',
        'settings' => array(
          'cycle_settings' => array(
            'cleartype' => '<default>',
            'cleartypeNoBg' => 0,
            'fit' => 0,
            'fx' => 'fade',
            'height' => 'auto',
            'manualTrump' => 1,
            'random' => 0,
            'startingSlide' => 0,
            'timeout' => 2000,
            'width' => '',
          ),
          'fancybox_grouping' => FALSE,
          'hide_next_prev_on_end' => 1,
          'image_style' => 'medium',
          'items_per_slide' => 1,
          'link' => FALSE,
          'link_destination' => 'file',
          'link_file_style' => 'large',
          'linked_field' => array(
            'advanced' => array(
              'class' => '',
              'rel' => '',
              'target' => '',
              'text' => '',
              'title' => '',
            ),
            'destination' => 'http://www.google.de/',
            'linked' => 0,
          ),
          'navigation' => 1,
          'nojs_behaviour' => 'start_slide_only',
          'pager' => 0,
          'pager_thumbnail_field' => 'field_slide_image',
          'pager_thumbnail_style' => 'thumbnail',
          'position_next_prev' => 'around_horizontal',
          'reset_on_resize' => 0,
          'show_index' => 0,
          'slide_on_click' => 0,
          'slide_on_hover' => 0,
          'use_fancybox' => FALSE,
          'use_thumbnail_pager' => 0,
          'view_mode' => 0,
        ),
        'type' => 'cycle',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => 'theme_ds_field_reset',
    'entity_type' => 'node',
    'field_name' => 'field_slide_content',
    'label' => 'Slides',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'field_collection',
      'settings' => array(),
      'type' => 'field_collection_embed',
      'weight' => 32,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Image');
  t('Link');
  t('Slides');
  t('Text');

  return $field_instances;
}
